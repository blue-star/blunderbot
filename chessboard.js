const emoji = require("./chessemoji.js");

class Chessboard
{
    board = null;
    moves = null;
    whitePlayerId = "";
    blackPlayerId = "";

    get whiteTurn ()
    {
        if (this.moves)
        {
            return this.moves.length % 2 == 0;
        }

        return null;
    }

    constructor ()
    {
        this.board = // board[y][x]
            [
                ["rook:black", "knight:black", "bishop:black", "queen:black", "king:black", "bishop:black", "knight:black", "rook:black"],
                ["pawn:black", "pawn:black", "pawn:black", "pawn:black", "pawn:black", "pawn:black", "pawn:black", "pawn:black"],
                [null, null, null, null, null, null, null, null],
                [null, null, null, null, null, null, null, null],
                [null, null, null, null, null, null, null, null],
                [null, null, null, null, null, null, null, null],
                ["pawn:white", "pawn:white", "pawn:white", "pawn:white", "pawn:white", "pawn:white", "pawn:white", "pawn:white"],
                ["rook:white", "knight:white", "bishop:white", "queen:white", "king:white", "bishop:white", "knight:white", "rook:white"]
            ];

        this.moves = [];
    }

    static parseJSON (json)
    {
        let board = new Chessboard();
        let parsed = JSON.parse(json);

        for (const [key, value] of Object.entries(parsed))
        {
            board[key] = value;
        }

        return board;
    }

    applyMoves (moves)
    {
        for (let i = 0; i < moves.length; i++)
        {
            this.applyMove(moves[i]);
        }
    }

    toXY (position)
    {
        let x = 0;
        let y = 8 - Number.parseInt(position[1]);

        if (position[0] == "a") x = 0;
        if (position[0] == "b") x = 1;
        if (position[0] == "c") x = 2;
        if (position[0] == "d") x = 3;
        if (position[0] == "e") x = 4;
        if (position[0] == "f") x = 5;
        if (position[0] == "g") x = 6;
        if (position[0] == "h") x = 7;

        return [x, y];
    }

    applyMove (move, playerId) // e.g. Bh4. playerId can be null if applyMoves() was called (though that function may be obsolete)
    {
        if (((playerId == this.whitePlayerId) == this.whiteTurn) || playerId == null)
        {
            switch (move.length)
            {
                case 1:
                    return false;
                case 2: //pawn ig
                    let xy = this.toXY(move);
                    this.board[xy[1]][xy[0]] = "pawn:" + (this.whiteTurn ? "white" : "black");
                    break;
                case 3:
                    //either a non-pawn move, or a pawn taking something
                    break;
                case 4:
                    //a non-pawn taking something, or a pawn taking something and checkmating?
                    break;
            }

            this.moves.push(move);

            //todo easy: put moved piece on new square
            //todo difficult: figure out which piece moved and whether it was legal
            //then todo easy: make old square blank
            return true;
        }

        return false;
    }

    outputEmojiBoard ()
    {
        let str = emoji(0, 0, "king", false) + "<@" + this.blackPlayerId + ">\n\n";

        for (let y = 0; y < 8; y++)
        {
            for (let x = 0; x < 8; x++)
            {
                let piece = this.board[y][x] ? this.board[y][x].split(":") : ["empty", "white"];

                str += emoji(x, y, piece[0], piece[1] == "white");
            }

            str += "\n";
        }

        str += "\n\n" + emoji(0, 0, "king", true) + "<@" + this.whitePlayerId + ">";

        return str;
    }
}

module.exports = Chessboard;