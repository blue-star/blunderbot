function emoji (x, y, piece, white)
{
    const emojis =
    {
        rook:
        {
            black:
            {
                blacksquare: "<:brb:1075868315446157412>",
                whitesquare: "<:brw:1075868790090375238>"
            },
            white:
            {
                blacksquare: "<:wrb:1075869050430836836>",
                whitesquare: "<:wrw:1075869051827519559>"
            }
        },

        knight:
        {
            black:
            {
                blacksquare: "<:bnb:1075868304490635424>",
                whitesquare: "<:bnw:1075868305962827937>"
            },
            white:
            {
                blacksquare: "<:wnb:1075868338623881246>",
                whitesquare: "<:wnw:1075869046492364951>"
            }
        },

        bishop:
        {
            black:
            {
                blacksquare: "<:bbb:1075868293157630063>",
                whitesquare: "<:bbw:1075868294617251990>"
            },
            white:
            {
                blacksquare: "<:wbb:1075868332185628682>",
                whitesquare: "<:wbw:1075868803558285353>"
            }
        },

        queen:
        {
            black:
            {
                blacksquare: "<:bqb:1075868785799594074>",
                whitesquare: "<:bqw:1075868788362326098>"
            },
            white:
            {
                blacksquare: "<:wqb:1075869048912486410>",
                whitesquare: "<:wqw:1075868345599012904>"
            }
        },

        king:
        {
            black:
            {
                blacksquare: "<:bkb:1075868297976877218>",
                whitesquare: "<:bkw:1075868300107583659>"
            },
            white:
            {
                blacksquare: "<:wkb:1075868336107311224>",
                whitesquare: "<:wkw:1075868807299604560>"
            }
        },

        pawn:
        {
            black:
            {
                blacksquare: "<:bpb:1075868309595115640>",
                whitesquare: "<:bpw:1075868310874361896>"
            },
            white:
            {
                blacksquare: "<:wpb:1075868810587934780>",
                whitesquare: "<:wpw:1075868342591684618>"
            }
        },

        empty:
        {
            blacksquare: "<:esb:1075868792762150994>",
            whitesquare: "<:esw:1075868794318233742>"
        },

        move:
        {
            blunder: "<:blunder:1075868302108270763>",
            mistake: "<:mistake:1075868802224492584>",
            brilliant: "<:brilliant:1075868317664948245>",
            good: "<:good:1075868324971413554>",
            best: "<:best:1075868296764731392>",
            great: "<:great:1075868798382506014>",
            excellent: "<:excellent:1075868321976684555>",
            inaccuracy: "<:inaccuracy:1075868800425132056>",
            missedwin: "<:missedwin:1075868329207677028>",
            forced: "<:forced:1075868796889354270>",
            book: "<:book:1075868307367927909>"
        },

        winner: "<:winner:1075868805395398677>"
    };

    let square = (x + (y % 2)) % 2 == 1 ? "whitesquare" : "blacksquare";

    piece = piece.toLowerCase().trim();

    if (!emojis[piece])
        console.log("not a piece:", piece);

    if (piece == "empty")
        return emojis[piece][square];
    else
        return emojis[piece][white ? "white" : "black"][square];
}

module.exports = emoji;