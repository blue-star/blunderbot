const TOKEN = "MTA3NTgwNzM0MjgzNjMxODM0MQ.GMbNq1.K2MCNMV_AjN3pRnLCAcRsZKOONBxHKa000aC_Q";
const CLIENT_ID = "1075807342836318341";
const URL = "https://discordapp.com/oauth2/authorize?&client_id=1075807342836318341&scope=bot&permissions=2147747840";

const { REST, Routes } = require('discord.js');
const emoji = require("./chessemoji.js");
const Chessboard = require("./chessboard.js");
var fs = require('fs');

const { Client, GatewayIntentBits } = require('discord.js');
const { randomBytes } = require('crypto');
const client = new Client({ intents: [GatewayIntentBits.Guilds] });

var data = {};

const commands = [
    {
        name: 'ping',
        description: 'Replies with Pong!',
        execute: function (interaction)
        {
            interaction.reply('Pong!');
        }
    },
    {
        name: 'update',
        description: 'Updates and reloads the bot!',
        execute: function (interaction)
        {
            interaction.reply('Updating!');
            please.crash.the.bot.now();
        }
    },
    {
        name: 'challenge',
        description: 'Challenge someone to a game!',
        options: [{ type: 6, name: "user", description: "The user to play against!", required: true }],
        execute: function (interaction)
        {
            let board = new Chessboard();

            let w = Math.random() > 0.5;
            board.whitePlayerId = w ? interaction.user.id : interaction.options.data[0].value;
            board.blackPlayerId = w ? interaction.options.data[0].value : interaction.user.id;

            createGame(interaction, board);

            interaction.reply("<@" + interaction.options.data[0].value + "> was challenged!\n" + board.outputEmojiBoard());
        }
    },
    {
        name: 'board',
        description: 'gib board!',
        options: [],
        execute: function (interaction)
        {
            let board = getGameForPlayer(interaction);

            interaction.reply(board.outputEmojiBoard());
        }
    },
    {
        name: 'move',
        description: 'Submit a move!',
        options: [{ type: 3, name: "move", description: "Your move", required: true }],
        execute: function (interaction)
        {
            let board = getGameForPlayer(interaction);

            let ok = board.applyMove(interaction.options.data[0].value, interaction.user.id);

            if (ok)
            {
                interaction.reply(interaction.options.data[0].value + "!\n" + board.outputEmojiBoard());

                let playerstring = data.servers[interaction.guildId].playergames[interaction.user.id];
                data.servers[interaction.guildId].games[interaction.channelId][playerstring] = JSON.stringify(board);
            }
            else
            {
                interaction.reply("It's not your turn!!!!!");
            }
        }
    }
];

function orderPlayerIds (w, b)
{
    let arr = [w, b];
    arr.sort();
    return arr;
}

function createGame (interaction, board)
{
    data.servers = data.servers || {};
    data.servers[interaction.guildId] = data.servers[interaction.guildId] || {};
    data.servers[interaction.guildId].games = data.servers[interaction.guildId].games || {};
    data.servers[interaction.guildId].games[interaction.channelId] = data.servers[interaction.guildId].games[interaction.channelId] || {};

    let playerstring = orderPlayerIds(board.whitePlayerId, board.blackPlayerId).join(":");

    data.servers[interaction.guildId].playergames = data.servers[interaction.guildId].playergames || {};
    data.servers[interaction.guildId].playergames[board.whitePlayerId] = playerstring;
    data.servers[interaction.guildId].playergames[board.blackPlayerId] = playerstring;

    data.servers[interaction.guildId].games[interaction.channelId][playerstring] = JSON.stringify(board);
}

function getGameForPlayer (interaction)
{
    data.servers = data.servers || {};
    data.servers[interaction.guildId] = data.servers[interaction.guildId] || {};
    data.servers[interaction.guildId].playergames = data.servers[interaction.guildId].playergames || {};

    return getGame(interaction, data.servers[interaction.guildId].playergames[interaction.user.id]);
}

function getGame (interaction, gameId)
{
    data.servers = data.servers || {};
    data.servers[interaction.guildId] = data.servers[interaction.guildId] || {};

    data.servers[interaction.guildId].games = data.servers[interaction.guildId].games || {};
    data.servers[interaction.guildId].games[interaction.channelId] = data.servers[interaction.guildId].games[interaction.channelId] || {};

    return Chessboard.parseJSON(data.servers[interaction.guildId].games[interaction.channelId][gameId]);
}

const rest = new REST({ version: '10' }).setToken(TOKEN);

(async () =>
{
    try
    {
        console.log('Started refreshing application (/) commands.');

        await rest.put(Routes.applicationCommands(CLIENT_ID), { body: commands });

        console.log('Successfully reloaded application (/) commands.');
    }
    catch (error)
    {
        console.error(error);
    }
})();

client.on('ready', () =>
{
    console.log(`Logged in as ${client.user.tag}!`);

    loadData(function (newData)
    {
        data = newData;
    });
});

function loadData (complete)
{
    fs.readFile('./botData.json', function read (err, data) 
    {
        if (err) 
        {
            data = "{}";
            console.log("no data found");
        }
        try
        {
            data = JSON.parse(data);
            console.log("savedata loaded:", data);
        }
        catch (e)
        {
            if (data == null)
            {
                console.log("Memory broken, will attempt to restore backup.");
            }
        }

        if (data == null || data == {})
        {
            fs.readFile('./botDataBackup.json', function read (err, data)
            {
                if (err)
                {
                    data = "{}";

                    console.log("no data found in backup either");
                }
                try
                {
                    data = JSON.parse(data);
                    console.log("Backup restored");
                }
                catch (e)
                {
                    if (data == null)
                    {
                        data = {};
                        console.log("Memory backup broken, making new");
                        saveBackup = false;
                    }
                }
                initialized = true;
            });
        }
        else //successfully loaded
        {
            client.saveBackup(true);

            initialized = true;
        }

        data = data || {};

        data.servers = data.servers || [];

        complete(data);
    });
}

client.on('interactionCreate', function (interaction)
{
    if (!interaction.isChatInputCommand()) return;

    for (let i = 0; i < commands.length; i++)
    {
        if (commands[i].name === interaction.commandName)
        {
            console.log("Current Data:", data);
            commands[i].execute(interaction);
            client.saveData();
            return;
        }
    }
});

client.saveBackup = function (silent = false)
{
    console.log("Writing Backup");
    fs.writeFile("./botDataBackup.json", JSON.stringify(data), function (err)
    {
        if (err)
        {
            console.log(err);
        }
    });
};

client.saveData = function (callback, context)
{
    console.log("Saving...");
    try
    {
        if (data != null && data != {} && initialized)
        {
            data = data || {};

            let json = JSON.stringify(data);

            fs.writeFile("./botData.json", json, function (err)
            {
                if (err)
                {
                    console.log(err);
                }

                console.log("Saved.", json);

                context = context || this;
                callback && callback.call(context, true);
            });
        }
    }
    catch (ex)
    {
        console.log("Error while saving!");
        console.log(ex);
        context = context || this;
        callback && callback.call(context, false);
    }
};

client.login(TOKEN);